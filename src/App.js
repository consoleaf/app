import React from 'react';
import connect from '@vkontakte/vkui-connect';
import {View} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import Home from './panels/Home';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activePanel: "home",
            fetchedUser: null,
            fetchedGeoData: {
                lat: 59.939045,
                lng: 30.315737
            }
        };
    }

    componentDidMount() {
        connect.subscribe((e) => {
            switch (e.detail.type) {
                case 'VKWebAppGetUserInfoResult':
                    this.setState({fetchedUser: e.detail.data});
                    break;
                case "VKWebAppGeodataResult":
                    this.setState({
                        fetchedGeoData: {
                            lat: e.detail.data.lat,
                            lng: e.detail.data.long
                        }
                    });
                    this.render();
                    break;
                default:
                    break;
            }
        });
        connect.send('VKWebAppGetUserInfo', {});
        connect.send("VKWebAppGetGeodata", {});
    }

    // Switch to panel which mentioned in "to" argument in event target.
    go = (e) => {
        this.setState({activePanel: e.currentTarget.dataset.to})
    };

    render() {
        return (
            <View activePanel={this.state.activePanel} className="mainView">
                <Home fetchedUser={this.state.fetchedUser} go={this.go} fetchedGeoData={this.state.fetchedGeoData}
                      locationsData={this.props.locationsData} locationsTypes={this.props.locationsTypes} id={"home"}/>
            </View>
        )
    }
}

export default App;