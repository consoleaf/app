import React from 'react';
import {GoogleApiWrapper, Map, Marker} from 'google-maps-react';
import ModalAdvancedAtmCard from "./ModalAdvancedAtmCard";
import {Spinner} from '@vkontakte/vkui';
import Images from "./res/Images"

class MapContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            chosenGeo: this.props.geoData,
            modalClosed: true,
            maximized: false,
            chosenLocation: {
                workNow: false,
                multicurrency: false,
                cashInsertion: false,
                payment: false,
                Location: {
                    Name: "undefined",
                    Distance: "undefined",
                    DistanceUnit: "mile",
                    Address: {
                        City: "undefined",
                        Line1: "undefined",
                        Line2: "undefined",
                        Country: {
                            Name: "undefined",
                        }
                    }
                }
            }
        };

        this.onMarkerClick = this.onMarkerClick.bind(this);
    }

    onMapClick() {
        document.getElementById("modalCard").classList.add("closed");
        document.getElementById("modalCard").classList.add("minimized");
        document.getElementById("modalCard").classList.remove("maximized");
    }

    maximizeModal() {
        document.getElementById("modalCard").classList.toggle("maximized");
        document.getElementById("modalCard").classList.toggle("minimized");
    }

    onMarkerClick(props) {
        console.log(props);
        this.setState({
            chosenLocation: props.data,
        });
        this.onMapClick();
        document.getElementById("modalCard").classList.remove("closed");
    }

    mkDetailedMarkers = () => {
        let markers = [];
        // let types = this.props.locationsTypes;
        //====
        for (let places in this.props.locationsData) {
            if (!this.props.locationsData.hasOwnProperty(places)) continue;
            let place = places.substring(0, places.length - 1);
            if (!this.props.locationsData[places].hasOwnProperty(place)) continue;
            let placeData = this.props.locationsData[places][place];
            // console.log(place, placeData);
            let childMarkers = placeData.map((placeData) =>
                <Marker
                    data={placeData}
                    onClick={this.onMarkerClick}
                    name={placeData.Location.Name}
                    position={{
                        lat: placeData.Location.Point.Latitude,
                        lng: placeData.Location.Point.Longitude
                    }}
                    icon={{
                        url: Images.getImage(place), //
                        anchor: new this.props.google.maps.Point(11.5, 32),
                        scaledSize: new this.props.google.maps.Size(23, 32) //TODO
                    }}
                    key={placeData.Location.Point.Latitude + '' + placeData.Location.Point.Longitude}
                />
            );
            markers.push(childMarkers)
        }
        // console.log(markers);
        return markers;
    };

    zoomHandler() {
        let zoom = window.map.getZoom();
        //TODO
        return zoom // avoiding warnings. Not needed.
    }

    fetchMap = function (mapProps, map) {
        window.map = map;
        map.addListener("zoom_changed", this.zoomHandler.bind(this));
        map.addListener("click", this.onMapClick.bind(this));
    }.bind(this);

    render() {
        // let data = this.props.locationsData["Atms"].Atm;
        let markers = this.mkDetailedMarkers();

        //data.map((atmData) =>
        //             <Marker
        //                 data={atmData}
        //                 onClick={this.onMarkerClick}
        //                 name={atmData.Location.Name}
        //                 position={{
        //                     lat: atmData.Location.Point.Latitude,
        //                     lng: atmData.Location.Point.Longitude
        //                 }}
        //                 icon={{
        //                     url: Images.atmLocation,
        //                     anchor: new this.props.google.maps.Point(11.5, 32),
        //                     scaledSize: new this.props.google.maps.Size(23, 32)
        //                 }}
        //                 key={atmData.Location.Point.Latitude + '' + atmData.Location.Point.Longitude}
        //             />
        //         );


        return (
            <React.Fragment>
                <Map data={this.props}
                     google={this.props.google}
                     zoom={14}
                     initialCenter={this.state.chosenGeo}
                     onReady={this.fetchMap}>
                    <Marker
                        name={'Current location'}
                        position={this.state.chosenGeo}
                        icon={{
                            url: Images.myLocation,
                            anchor: new this.props.google.maps.Point(12, 12),
                            scaledSize: new this.props.google.maps.Size(24, 24)
                        }}/>
                    {markers}
                </Map><ModalAdvancedAtmCard location={this.state.chosenLocation}
                                            closed={this.state.modalClosed}
                                            maximized={this.state.maximized}
                                            onClick={this.maximizeModal.bind(this)}
            /></React.Fragment>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyABiAQz9o60Q3tMi_Ei6I9U8eP1Xb6qbp4"),
    LoadingContainer: () => (<Spinner/>)
})(MapContainer)