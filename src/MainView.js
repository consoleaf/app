import React from 'react';
import {Spinner} from '@vkontakte/vkui';
// import AtmCard from "./AtmCard";
import AtmCardsGroup from "./AtmCardsGroup";
import MapContainer from "./MapContainer";

class MainView extends React.Component {
    render() {
        return (
            <div>
                {this.props.displayType === "list" && <div id="list">
                    <AtmCardsGroup locationsData={this.props.locationsData}
                                   locationsTypes={this.props.locationsTypes}/>
                </div>}
                {this.props.displayType === "map" && <div id="map">
                    <MapContainer geoData={this.props.fetchedGeoData} locationsData={this.props.locationsData}
                                  locationsTypes={this.props.locationsTypes}/>
                </div>}
                {this.props.displayType === "spinner" && <div id="spinner">
                    <Spinner/>
                </div>}
            </div>
        );
    }
}

export default MainView;