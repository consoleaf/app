class Images {
    static myLocation = require("./my_location.png");
    static bankLocation = require("./office.png");
    static atmLocation = require("./atm.png");
    static unknownLocation = require("./unknown_place.png");

    static getImage(placeName) {
        const images_dict = {
            "Atm": this.atmLocation,
            "Bank": this.bankLocation,
        };
        if (images_dict.hasOwnProperty(placeName))
            return images_dict[placeName];
        return this.unknownLocation;
    }
}

export default Images;