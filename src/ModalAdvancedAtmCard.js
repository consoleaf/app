import React from 'react';
import PropTypes from 'prop-types';
import {Div, InfoRow} from '@vkontakte/vkui';

class ModalAdvancedAtmCard extends React.Component {
    render() {
        return <div id={"modalCard"}
                    className="modalCard minimized closed"
                    onClick={this.props.onClick}>
            <div className="flex">
                <img src={require("./res/bank_photo.jpg")} alt=""/>
                <div className="growable">
                    <Div><b>{this.props.location.Location.Name}</b> {parseInt(this.props.location.Location.Distance * 10, 10) / 10} {this.props.location.Location.DistanceUnit.toLowerCase()}
                    </Div>
                    <Div>
                        <InfoRow title={"ADDRESS"}>
                            <div>
                                {this.props.location.Location.Address.Line1}
                            </div>
                            <div className={"additionalInfo"}>
                                {this.props.location.Location.Address.Country.Name}, {this.props.location.Location.Address.City}. {this.props.location.Location.Address.Line2}
                            </div>
                        </InfoRow>
                    </Div>
                </div>
            </div>
            <Div className="additionalInfo">
                <InfoRow title="MISC. INFO" className="additionalInfo">
                    <div>{this.props.location.workNow ? (<span className="green">Now open</span>) : (
                        <span className="red">Now closed</span>)}</div>
                    <div>{this.props.location.multicurrency ? (
                        <span className="green">Supports multiple currencies</span>) : (
                        <span className="red">Doesn't support multiple currencies</span>)}</div>
                    <div>{this.props.location.cashInsertion ? (
                        <span className="green">Can deposit/pay with cash</span>) : (
                        <span className="red">Can't deposit/pay with cash</span>)}</div>
                    <div>{this.props.location.payment ? (<span className="green">Can perform payments</span>) : (
                        <span className="red">Can't perform payments</span>)}</div>
                </InfoRow>
            </Div>
        </div>
    }
}

ModalAdvancedAtmCard.propTypes = {
    location: PropTypes.object.isRequired,
    additionalData: PropTypes.object
};

export default ModalAdvancedAtmCard;

/*
{
    "workNow": true,
    "multicurrency": true,
    "cashInsertion": false,
    "payment": false,
    "Location": {
        "Name": "Sandbox ATM Location 1",
        "Distance": 1.86,
        "DistanceUnit": "MILE",
        "Address": {
            "Line1": "438 Bingamon Branch Road",
            "Line2": "",
            "City": "Arlington Heights",
            "PostalCode": "11101",
            "CountrySubdivision": {
                "Name": "JACFGH",
                "Code": "GH"
            },
            "Country": {
                "Name": "Russia",
                "Code": "Russia"
            }
        },
        "Point": {
            "Latitude": 59.90600971237669,
            "Longitude": 30.492880515198344
        },
        "LocationType": {
            "Type": "OTHER"
        }
    },
    "HandicapAccessible": "NO",
    "Camera": "NO",
    "Availability": "UNKNOWN",
    "AccessFees": "UNKNOWN",
    "Owner": "Sandbox ATM 2",
    "SharedDeposit": "NO",
    "SurchargeFreeAlliance": "NO",
    "SurchargeFreeAllianceNetwork": "DOES_NOT_PARTICIPATE_IN_SFA",
    "Sponsor": "Sandbox",
    "SupportEMV": 1,
    "InternationalMaestroAccepted": 1
},
                    */