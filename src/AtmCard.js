import React from 'react';
import PropTypes from 'prop-types';
import {Cell, Group} from '@vkontakte/vkui';

const AtmCard = props => (
    <Group title={props.location.Name}>
        <Cell>{props.location.Distance} {props.location.DistanceUnit.toLowerCase()}s</Cell>
        <Cell>{props.location.Address.City},{props.location.Address.Line1}</Cell>
    </Group>
);

AtmCard.propTypes = {
	location: PropTypes.object.isRequired,
    additionalData: PropTypes.object
};

export default AtmCard;

/*("Location": {
    "Name": "Sandbox ATM Location 1",
        "Distance": 0.93,
        "DistanceUnit": "MILE",
        "Address": {
        "Line1": "186 Asylum Avenue",
            "Line2": "",
            "City": "Stamford",
            "PostalCode": "11101",
            "CountrySubdivision": {
            "Name": "JACFGH",
                "Code": "GH"
        },
        "Country": {
            "Name": "Russia",
                "Code": "Russia"
        }
    },
    "Point": {
        "Latitude": 59.927649888795955,
            "Longitude": 30.302407467692806
    },
    "LocationType": {
        "Type": "OTHER"
    }
},
"HandicapAccessible": "NO",
    "Camera": "NO",
    "Availability": "UNKNOWN",
    "AccessFees": "UNKNOWN",
    "Owner": "Sandbox ATM 1",
    "SharedDeposit": "NO",
    "SurchargeFreeAlliance": "NO",
    "SurchargeFreeAllianceNetwork": "DOES_NOT_PARTICIPATE_IN_SFA",
    "Sponsor": "Sandbox",
    "SupportEMV": 1,
    "InternationalMaestroAccepted": 1*/